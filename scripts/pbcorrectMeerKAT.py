#! /usr/bin/env python

from argparse import ArgumentParser
from simplepb import meerkat

def main():
    meerkat.cli()

if __name__ == "__main__":
    main()