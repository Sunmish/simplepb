#! /usr/bin/env python

from argparse import ArgumentParser
from simplepb import vla

def main():
    vla.cli()

if __name__ == "__main__":
    main()
