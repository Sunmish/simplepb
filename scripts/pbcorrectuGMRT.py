#! /usr/bin/env python

from argparse import ArgumentParser
from simplepb import ugmrt

def main():
    ugmrt.cli()

if __name__ == "__main__":
    main()
