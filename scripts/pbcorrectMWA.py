#! /usr/bin/env python

from argparse import ArgumentParser
from simplepb import mwa

def main():
    mwa.cli()

if __name__ == "__main__":
    main()
