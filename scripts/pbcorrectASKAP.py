#! /usr/bin/env python

from argparse import ArgumentParser
from simplepb import askap

def main():
    askap.cli()

if __name__ == "__main__":
    main()