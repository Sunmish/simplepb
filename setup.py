#! /usr/bin/env python

import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

reqs = [
    "astropy",
    "numpy",
    "reproject",
]

scripts = [
    "scripts/pbcorrectASKAP.py",
    "scripts/pbcorrectVLA.py",
    "scripts/pbcorrectMeerKAT.py",
    "scripts/pbcorrectuGMRT.py",
    "scripts/pbcorrectMWA.py"
]

setuptools.setup(
    name="simplepb",
    version="0.2.0",
    author="S Duchesne",
    author_email="stefanduchesne@gmail.com",
    description="Simple primary beam correction for some radio telescopes.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=reqs,
    packages=["simplepb"],
    scripts=scripts
)
