#! /usr/bin/env python

import numpy as np
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS

from simplepb import fitsutils

BANDS = {"P": (0.2, 0.5)}

class Band():
    def __init__(self, freq_range : tuple):
        self.freq_range = freq_range
        self.freqs = None
        self.a = None
    
    def find_nearest_freq(self, freq):
        if self.freqs is not None:
            idx = np.abs(self.freqs - freq).argmin()
            return idx
        else:
            raise ValueError("Band is not set up yet!")

def set_bands():
    """Set up band information."""

    PBand = Band((0.2, 0.5))
    PBand.freqs = np.array([232., 246., 281., 296., 312., 328., 344., 357., 382., 392., 403., 421., 458., 470.])/1e3 
    PBand.a = np.array([np.array([1. for f in PBand.freqs]), 
        np.array([-1.137, -1.13 , -1.106, -1.125, -1.03 , -0.98 , -0.974, -0.996, -1.002, -1.067, -1.057, -1.154, -0.993, -1.01 ])*1e-3,
        np.array([5.19, 5.04, 5.11, 5.27, 4.44, 4.25, 4.09, 4.23, 4.39, 5.13, 4.9, 5.85, 4.67, 4.85])*1e-7,
        np.array([-1.04, -1.02, -1.1 , -1.14, -0.89, -0.87, -0.76, -0.79, -0.88, -1.12, -1.06, -1.33, -1.04, -1.07])*1e-10,
        np.array([0.71, 0.77, 0.91, 0.96, 0.68, 0.69, 0.53, 0.51, 0.64, 0.9 , 0.87, 1.08, 0.88, 0.86])*1e-14
    ])

    bands = {"P": PBand}

    return bands

def get_band(freq : float, bands : list):

    for b in bands:
        if bands[b].freq_range[0] <= freq <= bands[b].freq_range[1]:
            return bands[b]


def even_poly(x : float, *args : float):
    # return a0 + a2*x**2 + a4*x**4 + a6*x**6 + a8*x**8
    print(args)
    return np.sum([args[i]*(x**(i*2)) for i in range(len(args))], axis=0)

def pbcorrect(image : str, 
    freq : float = None, 
    outname : str = None, 
    beam_outname : str = None,
    cutoff : float = None, 
    pcentre : tuple = None,
    trim : bool = False):
    """Make primary beam attenuation map then de-attenuate image. 

    If and outname is specified, the primary beam attenuation map will be 
    written out.

    Args:
        image (str): The file path to the image to PB correct.
        freq (float, optional): Frequency in GHz. If unset, read from the image.
        outname (str, optional): Output name for the PB map. If unset, PB map is 
            not written to disk.
        cutoff (float, optional): PB map values below this value are set to NaN. 
            By default no clipping is done.
        pcentre (tuple, optional): If PB is not centred on image, this should be
            specified as (RA, DEC) of the PB pointing centre.

     


    """

    bands = set_bands()

    with fits.open(image) as f:

        if freq is None:
            freq = fitsutils.fits_get_freq(f[0].header)/1.e9

        # Get band:
        band = get_band(freq, bands)
        
        wcs = WCS(f[0].header).celestial
        f[0].data = np.squeeze(f[0].data)
        f[0].header = fitsutils.strip_wcsaxes(f[0].header)

        if pcentre is None:
            ra, dec = f[0].header["CRVAL1"], f[0].header["CRVAL2"]
            coords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
        else:
            coords = SkyCoord(ra=pcentre*u.deg, dec=pcentre*u.deg)

        beam_image = f[0].data.copy()
        indices = np.indices(beam_image.shape)
        indices_x = indices[0]
        indices_y = indices[1]

        r, d = wcs.wcs_pix2world(indices_y, indices_x, 0)
        pixcoords = SkyCoord(ra=r*u.deg, dec=d*u.deg)
        seps = coords.separation(pixcoords)

        idx = band.find_nearest_freq(freq)
        vals = even_poly(seps.value*60.*freq, *[a[idx] for a in band.a])
        beam_image = vals.reshape(beam_image.shape).astype(np.float32)

        if cutoff is not None:
            beam_image[beam_image < cutoff] = np.nan
        
        if beam_outname is not None:
            fits.writeto(beam_outname, beam_image, f[0].header, overwrite=True)
            if trim:
                fitsutils.trim(beam_outname, overwrite=True)

        if outname is None:
            outname = image.replace(".fits", "") + "-pb.fits"

        fits.writeto(outname, (f[0].data / beam_image).astype(np.float32), f[0].header, overwrite=True)

        if trim:
            fitsutils.trim(outname, overwrite=True)

    
def cli():
    from argparse import ArgumentParser

    ps = ArgumentParser(description="Simple VLA primary beam correction of an image.")

    ps.add_argument("image", help="Image to PB correct.")
    ps.add_argument("-f", "--freq", type=float, default=None, 
        help="Frequency in GHz of image. If unset, will be read from the image.")
    ps.add_argument("-c", "--cutoff", default=None, type=float,
        help="PB map values below this value are set to NaN. By default no clipping is done.")
    ps.add_argument("-o", "--outname", default=None, type=str, 
        help="Output name for primary beam file. If unset not PB map is written to disk.")
    ps.add_argument("-t", "--trim", action="store_true",
        help="Switch to trim image to the primary beam clip.")

    args = ps.parse_args()

    pbcorrect(image=args.image,
        freq=args.freq,
        outname=args.outname,
        cutoff=args.cutoff,
        trim=args.trim)

