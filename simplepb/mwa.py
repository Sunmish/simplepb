#! /usr/bin/env pytnon

from typing import Optional

import numpy as np
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from simplepb import fitsutils

def radec_to_lm(ra, dec, ra0, dec0):
    """Convert RA,DEC to l,m."""

    l = np.cos(dec)*np.sin(ra-ra0)
    m = np.sin(dec)*np.cos(dec0) - np.cos(dec)*np.sin(dec0)*np.cos(ra-ra0)

    return l, m


def lm_to_radec(l, m, ra0, dec0):
    """Convert l,m to RA,DEC."""

    dec = np.arcsin(
        m*np.cos(dec0) + \
        np.sin(dec0)*np.sqrt(
            1 - (l**2) - (m**2)
        )
    )

    ra = ra0 + np.arctan(
        l / (
            np.cos(dec0)*np.sqrt(
                1 - (l**2) - (m**2)
            ) - m*np.sin(dec0)
        )
    )

    return ra, dec

def pbcorrect_with_beam_image(image : str,
    beam_model : str,
    outname : Optional[str] = None,
    beam_outname : Optional[str] = None,
    cutoff : Optional[float] = None,
    trim : bool = False):
    """
    De-attenuate MWA image based on input beam model image. 

    If a beam outname is specified, the primary beam attenuation map will be 
    written out.

    Args:
        image (str): The file path to the image to PB correct.
        metafits (str): Metafits file to get pointing direction.
        outname (str, optional): Output name for the PB-corrected map.
            If unset, default is to append -pb.fits to the image basename.
        beam_outname (str, optional): Output neam for the beam map that is 
            applied to the image (i.e. in the projection of the image). 
            If unset, no output beam map is written to disk.
        cutoff (float, optional): PB map values below this value are set to NaN. 
            By default no clipping is done.
        trim (bool): Trim output map to remove extra NaN pixels after using a cutoff.

    """


    image_hdu = fitsutils.strip_axes(fits.open(image))
    psf_info = [image_hdu[0].header["BMAJ"], image_hdu[0].header["BMIN"], image_hdu[0].header["BPA"]]

    if outname is None:
        outname = image.replace(".fits", "") + "-pb.fits"

    beam_hdu = fits.open(beam_model)

    regridded_beam = fitsutils.regrid_beam_map(
        beam_image=beam_hdu,
        image=image_hdu,
        # pointing=(pnt.ra.value, pnt.dec.value)
    )

    if cutoff is not None:
        regridded_beam[regridded_beam < cutoff] = np.nan

    if image_hdu[0].header["NAXIS"] == 2:
        image_hdu[0].data = (image_hdu[0].data / regridded_beam)

    else:
        image_hdu[0].data[..., :, :] = (image_hdu[0].data[..., :, :] / regridded_beam)

    if beam_outname is not None:
        fits.writeto(beam_outname, regridded_beam.astype(np.float32), fitsutils.strip_wcsaxes(image_hdu[0].header), 
            overwrite=True
        )
        if trim:
            fitsutils.trim(beam_outname, overwrite=True)

    if outname is None:
        if image.endswith(".fits"): 
            outname = image.replace(".fits", "-pb.fits")
        else:
            outname = image + ".pb"


    image_hdu[0].header["BMAJ"] = psf_info[0]
    image_hdu[0].header["BMIN"] = psf_info[1]
    image_hdu[0].header["BPA"] = psf_info[2]

    fits.writeto(outname, image_hdu[0].data.astype(np.float32), image_hdu[0].header, 
        overwrite=True
    )

    if trim:
        fitsutils.trim(outname, overwrite=True)


def cli():
    from argparse import ArgumentParser

    ps = ArgumentParser(description="Apply bespoke MWA beam model to image.")

    ps.add_argument("image", help="Image to PB correct.")
    ps.add_argument("beam_model", help="Beam model.")
    ps.add_argument("-c", "--cutoff", default=None, type=float,
        help="PB map values below this value are set to NaN. By default no clipping is done.")
    ps.add_argument("-o", "--outname", default=None, type=str)
    ps.add_argument("-bo", "--beam_outname", default=None, type=str, 
        help="Output name for primary beam file. If unset not PB map is written to disk.")
    ps.add_argument("-t", "--trim", action="store_true",
        help="Switch to trim image to the primary beam clip.")

    args = ps.parse_args()

    pbcorrect_with_beam_image(
        image=args.image,
        beam_model=args.beam_model,
        outname=args.outname,
        beam_outname=args.beam_outname,
        cutoff=args.cutoff,
        trim=args.trim
    )



