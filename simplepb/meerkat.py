#! /usr/bin/env python

import numpy as np
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS

from simplepb import fitsutils

def freq_scaling(freq):
    return 57.5*(1.5 / freq)

def attenuation(arcmin, freq):
    # From https://ui.adsabs.harvard.edu/abs/2020ApJ...888...61M/abstract


    theta_b = freq_scaling(freq)
    p_theta_b = arcmin / theta_b

    return (np.cos(1.189*np.pi*p_theta_b) / (1 - 4*(1.189 * p_theta_b)**2))**2



def collapse_deVilliers(antenna_averaged_file, frequency, outname=None):

    npz = np.load(antenna_averaged_file)
    freqs = npz["freq_MHz"]
    cellsize = np.diff(npz["margin_deg"])[0]
    data = npz["beam"]

    idx = (np.abs(freqs - frequency)).argmin()

    data_i = np.sqrt(np.sum(
        [np.abs(data[i, idx, ...])**2 for i in range(4)]
    , axis=0))

    hdu = fits.PrimaryHDU()
    hdu.header["CTYPE1"] = "RA---SIN"
    hdu.header["CRVAL1"] = 0.0
    hdu.header["CDELT1"] = -cellsize
    hdu.header["CRPIX2"] = 128 // 2 + 128%2
    hdu.header["CTYPE2"] = "DEC--SIN"
    hdu.header["CRVAL2"] = 0.0
    hdu.header["CDELT2"] = cellsize
    hdu.header["CRPIX1"] = 128 // 2 + 128%2
    hdu.header['EQUINOX'] = 2.000000000000E+03
    hdu.header['RADESYS'] = 'FK5     '
    hdu.header['SPECSYS'] = 'TOPOCENT'

    hdu.data = data_i
    if outname is not None:
        hdu.writeto(outname, overwrite=True)
    
    return hdu


def pbcorrect(image : str, 
    freq : float = None, 
    outname : str = None, 
    beam_outname : str = None,
    cutoff : float = None, 
    pcentre : tuple = None,
    trim : bool = False,
    beam_file : str = None):
    """Make primary beam attenuation map then de-attenuate image. 

    If and outname is specified, the primary beam attenuation map will be 
    written out.

    Args:
        image (str): The file path to the image to PB correct.
        freq (float, optional): Frequency in GHz. If unset, read from the image.
        outname (str, optional): Output name for the PB map. If unset, PB map is 
            not written to disk.
        cutoff (float, optional): PB map values below this value are set to NaN. 
            By default no clipping is done.
        pcentre (tuple, optional): If PB is not centred on image, this should be
            specified as (RA, DEC) of the PB pointing centre.

     


    """

    with fits.open(image) as f:

        if freq is None:
            freq = fitsutils.fits_get_freq(f[0].header)/1.e9
        
        wcs = WCS(f[0].header).celestial
        f[0].data = np.squeeze(f[0].data)
        f[0].header = fitsutils.strip_wcsaxes(f[0].header)

        if pcentre is None:
            ra, dec = f[0].header["CRVAL1"], f[0].header["CRVAL2"]
            coords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
        else:
            coords = SkyCoord(ra=pcentre[0]*u.deg, dec=pcentre[1]*u.deg)

        beam_image = f[0].data.copy()
        indices = np.indices(beam_image.shape)
        indices_x = indices[0]
        indices_y = indices[1]


        if beam_file is None:
            r, d = wcs.wcs_pix2world(indices_y, indices_x, 0)
            pixcoords = SkyCoord(ra=r*u.deg, dec=d*u.deg)
            seps = coords.separation(pixcoords)

            vals = attenuation(seps.value*60, freq)
            beam_image = vals.reshape(beam_image.shape).astype(np.float32)

        else:
            beam_hdu = collapse_deVilliers(
                antenna_averaged_file=beam_file,
                frequency=freq*1e3,
                outname=None
            )

            beam_image = fitsutils.regrid_beam_map(
                beam_image=beam_hdu,
                image=f,
                pointing=(coords.ra.value, coords.dec.value)
            )

        if cutoff is not None:
            beam_image[beam_image < cutoff] = np.nan
        
        if beam_outname is not None:
            fits.writeto(beam_outname, beam_image, f[0].header, overwrite=True)
            if trim:
                fitsutils.trim(beam_outname, overwrite=True)


        if outname is None:
            outname = image.replace(".fits", "") + "-pb.fits"

        f[0].header["FREQ"] = freq*1e9

        fits.writeto(outname, (f[0].data / beam_image).astype(np.float32), f[0].header, overwrite=True)

        if trim:
            fitsutils.trim(outname, overwrite=True)
            


def cli():
    from argparse import ArgumentParser

    ps = ArgumentParser(description="Simple MeerKAT primary beam correction of an image.")

    ps.add_argument("image", help="Image to PB correct.")
    ps.add_argument("-f", "--freq", type=float, default=None, 
        help="Frequency in GHz of image. If unset, will be read from the image.")
    ps.add_argument("-c", "--cutoff", default=None, type=float,
        help="PB map values below this value are set to NaN. By default no clipping is done.")
    ps.add_argument("-o", "--outname", default=None, type=str)
    ps.add_argument("-bo", "--beam_outname", default=None, type=str, 
        help="Output name for primary beam file. If unset not PB map is written to disk.")
    ps.add_argument("-t", "--trim", action="store_true",
        help="Switch to trim image to the primary beam clip.")
    ps.add_argument("-B", "--beam_file", type=str, default=None,
        help="Numpy file containing an antenna-averaged beam model from de Velliers+2023.")
    ps.add_argument("-p", "--pointing", nargs=2, default=None, type=float,
        help="Pointing centre in degrees if not at image centre.")

    args = ps.parse_args()

    pbcorrect(image=args.image,
        freq=args.freq,
        outname=args.outname,
        beam_outname=args.beam_outname,
        cutoff=args.cutoff,
        trim=args.trim,
        beam_file=args.beam_file,
        pcentre=args.pointing
    )