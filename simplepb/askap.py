#! /usr/bin/env python

import numpy as np
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS
from astropy.constants import c; c = c.value

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from simplepb import fitsutils


def fwhm_to_sigma(fwhm):
    return fwhm / (2.*np.sqrt(2.*np.log(2.)))


def gaussian_2d(x, y, x0, y0, sigma, A=1.):
    return A*np.exp(-(1./(2.*sigma**2))*((x-x0)**2 + (y-y0)**2))


def elliptical_gaussian_2d(x, y, x0, y0, sigma_x, sigma_y, theta=0, A=1):
    xp = (x-x0)*np.cos(theta) - (y-y0)*np.sin(theta)
    yp = (x-x0)*np.sin(theta) + (y-y0)*np.cos(theta)
    return A*np.exp(-(
        (xp**2 / (2*sigma_x**2)) + \
        (yp**2 / (2*sigma_y**2))
    ))


def extract_beam_model(beam_hdu, beam_number, frequency, stokes=0):
    """Extract beam model for a given frequency from beam cube."""

    beam_freqs = np.array([
        beam_hdu[0].header["CRVAL3"] + channel*beam_hdu[0].header["CDELT3"] for
        channel in range(beam_hdu[0].data.shape[2])
    ])
    logger.debug(beam_freqs)

    idx = (np.abs(beam_freqs - frequency)).argmin()
    logger.debug("{} closest frequency: {}".format(frequency, beam_freqs[idx]))

    beam_image = beam_hdu[0].data[beam_number, stokes, idx, :, :]
    beam_header = fitsutils.strip_wcsaxes(beam_hdu[0].header)

    beam_hdu[0].data = beam_image
    beam_hdu[0].header = beam_header

    return beam_hdu


def pbcorrect(image : str,
    freq : float = None,
    d : float = 12.,
    fac : float = 1.09,
    double : bool = False, 
    outname : str = None, 
    beam_outname : str = None,
    cutoff : float = None, 
    pcentre : tuple = None,
    trim : bool = False,
    template : str = None):
    """Make primary beam attenuation map then de-attenuate image. 

    If and outname is specified, the primary beam attenuation map will be 
    written out.

    Args:
        image (str): The file path to the image to PB correct.
        freq (float, optional): Frequency in GHz. If unset, read from the image.
        d (float, optional): Dish diameter in m for ASKAP. Default 12.
        fac (float, optional): Factor for calculation of Gaussian FWHM. Default 1.09.
        double (bool, optional): Double output total intensity image.
        outname (str, optional): Output name for the PB map. If unset, PB map is 
            not written to disk.
        cutoff (float, optional): PB map values below this value are set to NaN. 
            By default no clipping is done.
        pcentre (tuple, optional): If PB is not centred on image, this should be
            specified as (RA, DEC) of the PB pointing centre.

    """

    with fits.open(image) as f:

        

        if freq is None:
            freq = fitsutils.fits_get_freq(f[0].header)
        wl = c / freq

        f[0].data = np.squeeze(f[0].data)
        f[0].header = fitsutils.strip_wcsaxes(f[0].header)
        psf_info = [f[0].header["BMAJ"], f[0].header["BMIN"], f[0].header["BPA"]]
        fwhm = np.degrees(fac*(wl/d))
        sigma = fwhm_to_sigma(fwhm)/fitsutils.fits_get_pixscale(f[0].header)


        if pcentre is None:
            crpix1, crpix2 = f[0].header["CRPIX1"], f[0].header["CRPIX2"]
        else:
            coords = SkyCoord(ra=pcentre[0]*u.deg, dec=pcentre[1]*u.deg)
            wcs = WCS(f[0].header).celestial
            crpix2, crpix1 = wcs.wcs_world2pix(coords.ra.value, coords.dec.value, 0)
       
        beam_image = f[0].data.copy()

        indices = np.indices(beam_image.shape)
        indices_x = indices[0]
        indices_y = indices[1]

        beam_image[indices_x, indices_y] = gaussian_2d(indices_x, indices_y, crpix1, crpix2, sigma)



        if template is not None:
            with fits.open(template) as t:
                beam_image, _ = fitsutils.regrid_map(
                    image_data=beam_image,
                    image_header=f[0].header,
                    template=t
                )
                image_data, image_header = fitsutils.regrid_map(
                    image=f,
                    template=t
                )
                f[0].header = image_header
                f[0].data = image_data
                

        
        

        if cutoff is not None:
            beam_image[beam_image < cutoff] = np.nan

        if beam_outname is not None:
            fits.writeto(beam_outname, beam_image, f[0].header, overwrite=True)
            if trim:
                fitsutils.trim(beam_outname, overwrite=True)
        
        if outname is None:
            outname = image.replace(".fits", "") + "-pb.fits"

        if double:
            dfac = 2.
        else:
            dfac = 1.

        f[0].header["BMAJ"] = psf_info[0]
        f[0].header["BMIN"] = psf_info[1]
        f[0].header["BPA"] = psf_info[2]


        fits.writeto(outname, (f[0].data / beam_image)*dfac, f[0].header, overwrite=True)

        if trim:
            fitsutils.trim(outname, overwrite=True)


def pbcorrect_vleakage(image_v, image_i, beam_image_i, beam_image_v,
    cutoff_i=None,
    outname=None,
    beam_outname=None,
    double=False,
    pcentre=None):

    image_i_hdu = fitsutils.strip_axes(fits.open(image_i))
    beam_i_hdu = fitsutils.strip_axes(fits.open(beam_image_i))
    image_v_hdu = fitsutils.strip_axes(fits.open(image_v))
    beam_v_hdu = fitsutils.strip_axes(fits.open(beam_image_v))

    if pcentre is None:
        crval1 = image_i_hdu[0].header["CRVAL1"]
        crval2 = image_i_hdu[0].header["CRVAL2"]
    else:
        crval1, crval2 = pcentre
    
    if (pcentre is None) and \
        (image_i_hdu[0].header["NAXIS1"] == beam_i_hdu[0].header["NAXIS1"]) and \
        (image_i_hdu[0].header["NAXIS2"] == beam_i_hdu[0].header["NAXIS2"]):
        regridded_beam_i = beam_i_hdu[0].data
        regridded_beam_v = beam_v_hdu[0].data
    else: 
        regridded_beam_i = fitsutils.regrid_beam_map(
                beam_image=beam_i_hdu, 
                image=image_i_hdu, 
                pointing=(crval1, crval2)
        )
        regridded_beam_v = fitsutils.regrid_beam_map(
                beam_image=beam_v_hdu, 
                image=image_v_hdu, 
                pointing=(crval1, crval2)
        )

    if cutoff_i is not None:
        regridded_beam_v[regridded_beam_i < cutoff_i] = np.nan
        regridded_beam_i[regridded_beam_i < cutoff_i] = np.nan

    if double:
        dfac = 2.
    else:
        dfac = 1.

    if image_i_hdu[0].header["NAXIS"] == 2:
        image_i_hdu[0].data = (image_i_hdu[0].data / regridded_beam_i) * dfac
        image_v_hdu[0].data = image_v_hdu[0].data*dfac / regridded_beam_i - image_i_hdu[0].data*regridded_beam_v

    else:
        image_i_hdu[0].data[..., :, :] = (image_i_hdu[0].data[..., :, :] / regridded_beam_i) * dfac
        image_v_hdu[0].data[..., :, :] = image_v_hdu[0].data[..., :, :]*dfac - image_i_hdu[0].data[..., :, :]*regridded_beam_v
    
    if beam_outname is not None:
        fits.writeto(beam_outname, regridded_beam_v, fitsutils.strip_wcsaxes(image_v_hdu[0].header), 
            overwrite=True
        )

    if outname is None:
        if image_v.endswith(".fits"): 
            outname = image_v.replace(".fits", "-pb.fits")
        else:
            outname = image_v + ".pb"

    fits.writeto(outname, image_v_hdu[0].data, image_v_hdu[0].header, 
        overwrite=True
    )


def pbcorrect_from_beam_image(image, beam_image,
    cutoff=None, 
    outname=None,
    beam_outname=None,
    double=False, 
    pcentre=None,
    normalize=False,
    beam_number=None,
    freq=None,
    trim=False,
    template=None
    ):


    image_hdu = fitsutils.strip_axes(fits.open(image))
    psf_info = [image_hdu[0].header["BMAJ"], image_hdu[0].header["BMIN"], image_hdu[0].header["BPA"]]

    if pcentre is None:
        crval1 = image_hdu[0].header["CRVAL1"]
        crval2 = image_hdu[0].header["CRVAL2"]
    else:
        crval1, crval2 = pcentre

    if template is not None:
        with fits.open(template) as f:
            image_data, image_header = fitsutils.regrid_map(
                image=image_hdu,
                template=f
            )
            image_hdu[0].header = image_header
            image_hdu[0].data = image_data
    
    if beam_number is None:
        # assume 2d image of the primary beam for a single PAF beam
        beam_hdu = fitsutils.strip_axes(fits.open(beam_image))
    else:
        # assume holography cube with all 36 beams - need to then slice into it
        if freq is None:
            freq = fitsutils.fits_get_freq(fits.getheader(image))
        beam_hdu = extract_beam_model(
            beam_hdu=fits.open(beam_image),
            beam_number=beam_number,
            frequency=freq
        )

    if (pcentre is None) and \
        (image_hdu[0].header["NAXIS1"] == beam_hdu[0].header["NAXIS1"]) and \
        (image_hdu[0].header["NAXIS2"] == beam_hdu[0].header["NAXIS2"]) and  \
        (image_hdu[0].header["CRVAL1"] == beam_hdu[0].header["CRVAL1"]) and \
        (image_hdu[0].header["CRVAL2"] == beam_hdu[0].header["CRVAL2"]):
        regridded_beam = beam_hdu[0].data
    else: 
        regridded_beam = fitsutils.regrid_beam_map(
                beam_image=beam_hdu, 
                image=image_hdu, 
                pointing=(crval1, crval2)
        )

    if cutoff is not None:
        regridded_beam[regridded_beam < cutoff] = np.nan
    if normalize:
        regridded_beam /= np.nanmax(regridded_beam)
    
    
    if double:
        dfac = 2.
    else:
        dfac = 1.

    if image_hdu[0].header["NAXIS"] == 2:
        image_hdu[0].data = (image_hdu[0].data / regridded_beam) * dfac

    else:
        image_hdu[0].data[..., :, :] = (image_hdu[0].data[..., :, :] / regridded_beam) * dfac
    
    if beam_outname is not None:
        fits.writeto(beam_outname, regridded_beam.astype(np.float32), fitsutils.strip_wcsaxes(image_hdu[0].header), 
            overwrite=True
        )
        if trim:
            fitsutils.trim(beam_outname, overwrite=True)

    if outname is None:
        if image.endswith(".fits"): 
            outname = image.replace(".fits", "-pb.fits")
        else:
            outname = image + ".pb"


    image_hdu[0].header["BMAJ"] = psf_info[0]
    image_hdu[0].header["BMIN"] = psf_info[1]
    image_hdu[0].header["BPA"] = psf_info[2]

    fits.writeto(outname, image_hdu[0].data.astype(np.float32), image_hdu[0].header, 
        overwrite=True
    )

    if trim:
        fitsutils.trim(outname, overwrite=True)



def cli():
    from argparse import ArgumentParser

    ps = ArgumentParser(description="Simple ASKAP 2-d Gaussian primary beam correction of an image.")

    ps.add_argument("image", help="Image to PB correct.")
    ps.add_argument("-f", "--freq", type=float, default=None, 
        help="Frequency in GHz of image. If unset, will be read from the image.")
    ps.add_argument("-c", "--cutoff", default=None, type=float,
        help="PB map values below this value are set to NaN. By default no clipping is done.")
    ps.add_argument("-o", "--outname", default=None, type=str)
    ps.add_argument("-bo", "--beam_outname", default=None, type=str, 
        help="Output name for primary beam file. If unset not PB map is written to disk.")
    ps.add_argument("-d", "--double", action="store_true", 
        help="Double Stokes I image - due to a difference in Stokes convention " \
             "ASKAP images produced with e.g. WSClean with data taken from CASDA "
             "will have a total intensity half what is expected.")
    ps.add_argument("-p", "--pointing", nargs=2, default=None, type=float,
        help="Pointing centre in degrees if not at image centre.")
    ps.add_argument("-B", "--beam_image", type=str, default=None,
        help="Image of primary beam response to use instead of 2D Gaussian.")
    ps.add_argument("-N", "--normalize", action="store_true",
        help="Normalize the beam image prior to de-attenuation.")
    ps.add_argument("-v", "--vimage", 
        default=None,
        help="Stokes V image for leakage correction.")
    ps.add_argument("-vB", "--vbeam_image",
        default=None,
        help="Stokes V leakage image.")
    ps.add_argument("-n", "--beam_number", "--beam-number", "--number", 
        dest="beam_number",
        type=int,
        default=None,
        help="Beam number in footprint if using holography model."
    )
    ps.add_argument("-t", "--trim", action="store_true",
        help="Switch to trim image to the primary beam clip.")
    ps.add_argument("--template", default=None,
        help="Template image to regrid main image to.")

    args = ps.parse_args()

    if args.freq is not None:
        args.freq *= 1.e9  # expects Hz

    if args.vbeam_image is not None:
        pbcorrect_vleakage(
            image_i=args.image,
            beam_image_i=args.beam_image,
            image_v=args.vimage,
            beam_image_v=args.vbeam_image,
            cutoff_i=args.cutoff,
            double=args.double,
            outname=args.outname,
            beam_outname=args.beam_outname,
            pcentre=args.pointing
        )

    if args.beam_image:
        pbcorrect_from_beam_image(
            image=args.image,
            beam_image=args.beam_image,
            cutoff=args.cutoff,
            double=args.double,
            outname=args.outname,
            beam_outname=args.beam_outname,
            pcentre=args.pointing,
            normalize=args.normalize,
            freq=args.freq,
            beam_number=args.beam_number,
            trim=args.trim,
            template=args.template
        )
    else:
        pbcorrect(
            image=args.image,
            freq=args.freq,  
            outname=args.outname,
            beam_outname=args.beam_outname,
            cutoff=args.cutoff,
            double=args.double,
            pcentre=args.pointing,
            trim=args.trim,
            template=args.template
        )