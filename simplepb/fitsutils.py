#! /usr/bin/env python

import numpy as np
from argparse import ArgumentParser
from astropy.io import fits
from astropy.constants import c; c = c.value
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS

from scipy import ndimage

import reproject

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def minimal_bounding_box(image_data):
    """
    Obtain minimal bounding box for an image.
    Based on https://stackoverflow.com/a/31402351

    """

    arr = np.where(np.isfinite(image_data))
    bbox = np.min(arr[0]), np.max(arr[0]), np.min(arr[1]), np.max(arr[1])
    return bbox


def blank_islands(image_data):
    """Blank islands of pixels using scipy.ndimage.label."""

    logger.debug("Retyping image...")
    # arr = np.squeeze(image_data).copy().byteswap().newbyteorder().astype("float64")
    
    arr = np.squeeze(image_data).copy().astype("float64")
    # mask islands, remove nans
    logger.debug("Masking...")
    arr[np.where(np.isfinite(arr))] = 1.
    arr[np.where(np.isnan(arr))] = 0.

    logger.debug("Labelling array...")
    source_image, _ = ndimage.label(arr)
    sources = list(set(source_image.flatten()))
    try:
        sources.remove(0)
    except ValueError:
        # no trimming possible or needed
        raise

    logger.debug("Number of islands: {}".format(len(sources)))
    if len(sources) > 1:  # 
        logger.debug("Removing extra {} island(s)...".format(len(sources)-1))
        # little islands detected!
        lens = []
        for island in sources:
            lens.append(len(np.where(source_image.flatten() == island)[0]))
        good_island_idx = np.argmax(lens)
        # main_image = sources[np.argmax(lens)]
        # image_data[..., np.where(source_image != main_image)] = np.nan

        for island in sources:
            if island != sources[good_island_idx]:
                idx = np.where(source_image == island)
                image_data[..., idx[0], idx[1]] = np.nan
        islands_blanked = True
    else:
        islands_blanked = False

    return image_data, islands_blanked


def trim(image, 
    overwrite=False):
    """Trim images."""

    with fits.open(image) as f:

        original_crpix = f[0].header["CRPIX1"], f[0].header["CRPIX2"]

        # trim errant islands:
        logger.debug("Blanking islands...")
        try:
            f[0].data, islands_blanked = blank_islands(f[0].data)
        except ValueError:
            logger.warning("No trimming possible/necesary.")

        # constrict to a minimal rectangular bounding box:
        logger.debug("Trimming excess image...")
        bbox = minimal_bounding_box(np.squeeze(f[0].data))
        
        f[0].data = f[0].data[..., bbox[0]:bbox[1], bbox[2]:bbox[3]]

        # move reference pixels to match new array:
        xdiff = original_crpix[0] - bbox[2]
        ydiff = original_crpix[1] - bbox[0]
        f[0].header["CRPIX1"] = xdiff
        f[0].header["CRPIX2"] = ydiff

 
        f.writeto(image, overwrite=overwrite)


def fits_get_freq(header : fits.Header):
    """Find frequency information in a FITS header.
    
    Checks a number of common locations.
    """

    freq = None
    if "FREQ" in header.keys():
        freq = header["FREQ"]
    for i in ["3", "4"]:
        if (freq is None) and ("CRVAL"+i in header.keys()):
            if "FREQ" in header["CTYPE"+i]:
                freq = header["CRVAL"+i]
    if (freq is None) and ("CENTCHAN" in header.keys()):
        freq = 1.28*header["CENTCHAN"]*1.e6  # specific to MWA data

    if freq is None:
        raise ValueError("unable to determine frequency information.")

    if freq < 1000.:
        freq *= 1.e6

    return freq


def fits_get_pixscale(header : fits.Header):
    """Find pixel scale information in a FITS header.
    
    Checks a number of common locations.
    """

    
    if "CD1_1" in header.keys():
        cd = abs(header["CD1_1"])
    else:
        cd = abs(header["CDELT1"])

    return cd


def strip_wcsaxes(header : fits.Header):
    """Strips non-celestial axes."""

    remove_keys = [key+i for key in ["CRVAL", "CDELT", "CRPIX", "CTYPE", "CUNIT", "NAXIS"]
                         for i in ["3", "4", "5"]]

    for key in remove_keys:
        if key in header.keys():
            del header[key]

    return header


def strip_axes(hdu):
    try:
        freq = fits_get_freq(hdu[0].header)
    except ValueError:
        freq = None
    hdu[0].data = np.squeeze(hdu[0].data)
    remove_keys = [key+i for key in 
                   ["CRVAL", "CDELT", "CRPIX", "CTYPE", "CUNIT", "NAXIS"]
                   for i in ["3", "4"]]
    for key in remove_keys:
        if key in hdu[0].header.keys():
            del hdu[0].header[key]
    
    if freq is not None:
        hdu[0].header["FREQ"] = freq
    return hdu


def regrid_beam_map(beam_image : fits.HDUList, image : fits.HDUList,
    pointing=None):
    """Regrid a primary beam image to match a science image."""

    image_header = strip_wcsaxes(image[0].header.copy())
    if pointing is None:
        crval1 = image_header["CRVAL1"]
        crval2 = image_header["CRVAL2"]
    else:
        crval1, crval2 = pointing

    try:
        beam_data = beam_image[0].data.copy()
        beam_header = beam_image[0].header.copy()
    except TypeError:
        beam_data = beam_image.data.copy()
        beam_header = beam_image.header.copy()

    beam_header["CRVAL1"] = crval1 
    beam_header["CRVAL2"] = crval2

    image_wcs = WCS(image_header).celestial

    regridded_data, _ = reproject.reproject_interp(
        input_data=(beam_data, beam_header),
        output_projection=image_wcs,
        shape_out=(image[0].data.shape[-1], image[0].data.shape[-2])
    )

    return np.nan_to_num(regridded_data)


def regrid_map(template : fits.HDUList, image : fits.HDUList = None, 
    image_header = None,
    image_data = None):
    """Regrid image to other image."""

    template_header = strip_wcsaxes(template[0].header.copy())
    crval1 = template_header["CRVAL1"]
    crval2 = template_header["CRVAL2"]

    if image is not None:
        image_data = image[0].data.copy()
        image_header = image[0].header.copy()

    # image_header["CRVAL1"] = crval1 
    # image_header["CRVAL2"] = crval2

    template_wcs = WCS(template_header).celestial

    regridded_data, _ = reproject.reproject_interp(
        input_data=(image_data, image_header),
        output_projection=template_wcs,
        shape_out=(template[0].data.shape[-1], template[0].data.shape[-2])
    )

    return regridded_data, template_header

