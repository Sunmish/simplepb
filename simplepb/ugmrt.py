#! /usr/bin/env python

"""
http://www.ncra.tifr.res.in/ncra/gmrt/gmrt-users/observing-help/ugmrt-primary-beam-shape
"""

import numpy as np
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS
from astropy.constants import c; c = c.value

from simplepb import fitsutils

def polynomial(x : float, a : float, b : float, c : float, d : float):
    return 1 + \
        (a/1e3)*x**2 + \
        (b/1e7)*x**4 + \
        (c/1e10)*x**6 + \
        (d/1e13)*x**8

def get_coefficients_yr2018(frequency : float):
    """
    http://www.ncra.tifr.res.in/ncra/gmrt/gmrt-users/observing-help/ugmrt-primary-beam-shape
    """

    if 125 <= frequency < 250:
        raise RuntimeError("No coefficients for Band-2 in the 2018 parameter set. Try 2023.")
    elif 250 <= frequency <= 500:
        a, b, c, d = -2.939, 33.312, -16.659, 3.066
    elif 550 <= frequency < 850:
        a, b, c, d = -3.190, 38.642, -20.471, 3.964
    elif 1050 <= frequency < 1450:
        a, b, c, d = -2.608, 27.357, -13.091, 2.368
    else:
        raise RuntimeError("No coefficients for {} MHz.".format(frequency))
    
    return a, b, c, d

def get_coefficients_yr2022(frequency : float):
    """
    http://www.ncra.tifr.res.in/ncra/gmrt/gmrt-users/beam-shape-v1-09sep2022.pdf
    """

    if 125 <= frequency < 250:
        a, b, c, d = -3.089, 39.314, -23.011, 5.037
    elif 250 <= frequency <= 500:
        a, b, c, d = -2.939, 33.312, -16.659, 3.066  # same as 2018 set
    elif 550 <= frequency < 850:
        a, b, c, d = -3.263, 42.618, -25.580, 5.823
    elif 1050 <= frequency < 1450:
        a, b, c, d = -2.614, 27.594, -13.268, 2.395
    else:
        raise RuntimeError("No coefficients for {} MHz.".format(frequency))
    
    return a, b, c, d

def get_coefficients_yr2023(frequency : float):
    """
    http://www.ncra.tifr.res.in/ncra/gmrt/gmrt-users/beam-shape-v1-29nov2023.pdf
    """

    # the same as 2023 except with band-3 updated
    if 125 <= frequency < 250:
        a, b, c, d = -3.089, 39.314, -23.011, 5.037
    elif 250 <= frequency <= 500:
        a, b, c, d = -3.129, 38.816, -21.608, 4.483  
    elif 550 <= frequency < 850:
        a, b, c, d = -3.263, 42.618, -25.580, 5.823
    elif 1050 <= frequency < 1450:
        a, b, c, d = -2.614, 27.594, -13.268, 2.395
    else:
        raise RuntimeError("No coefficients for {} MHz.".format(frequency))
    
    return a, b, c, d

def evaluate_beam(frequency : float, distance : float, year : str = 2023):

    if year == "2023":
        a, b, c, d = get_coefficients_yr2023(frequency*1000.)
    elif year == "2022":
        a, b, c, d = get_coefficients_yr2022(frequency*1000.)
    elif year == "2018":
        a, b, c, d = get_coefficients_yr2018(frequency*1000.)
    else:
        raise RuntimeError("No model for year {} - try 2018, 2022, or 2023.".format(year))
    
    return polynomial(frequency*distance, a, b, c, d)


def pbcorrect(image : str,
    freq : float = None,
    outname : str = None,
    beam_outname : str = None,
    cutoff : float = None,
    pcentre : tuple = None,
    trim : bool = False,
    model : str = "2023"
    ):
    """Make primary beam attenuation map then de-attenuate image.

    Args:
        image (str): The file path to the image to PB correct.
        freq (float, optional): Frequency in GHz. If unset, read from the image.
        outname (str, optional): Output file name for PB-corrected image.
        beam_outname (str, optional): Set to output the beam image as well.
        cutoff (float, optional): PB map values below this value are set to NaN. 
            By default not clipping is done.
        pcentre (tuple, optional): If PB is not centred on image, this should be
            specified as (RA, DEC) of the PB pointing centre.

    """

    with fits.open(image) as f:
        
        if freq is None:
            freq = fitsutils.fits_get_freq(f[0].header) / 1e9

        f[0].data = np.squeeze(f[0].data)    
        f[0].header = fitsutils.strip_wcsaxes(f[0].header)
        wcs = WCS(f[0].header).celestial

        if pcentre is None:
            crval1, crval2 = f[0].header["CRVAL1"], f[0].header["CRVAL2"]
        else:
            crval1, crval2 = pcentre
        
        coords = SkyCoord(ra=crval1*u.deg, dec=crval2*u.deg)
        
        beam_image = f[0].data.copy()
        indices = np.indices(beam_image.shape)
        indices_x = indices[0]
        indices_y = indices[1]

        r, d = wcs.wcs_pix2world(indices_y, indices_x, 0)
        pixcoords = SkyCoord(ra=r*u.deg, dec=d*u.deg)
        seps = coords.separation(pixcoords).value * 60.  # arcmin

        vals = evaluate_beam(freq, seps, year=model)

        beam_image = vals.reshape(beam_image.shape)

        if cutoff is not None:
            beam_image[beam_image < cutoff] = np.nan
        
        if beam_outname is not None:
            fits.writeto(beam_outname, beam_image.astype(np.float32), f[0].header, overwrite=True)
            if trim:
                fitsutils.trim(beam_outname, overwrite=True)

        if outname is None:
            outname = image.replace(".fits", "-pb.fits")
        
        f[0].header["FREQ"] = freq*1e9
        fits.writeto(outname, (f[0].data / beam_image).astype(np.float32), f[0].header, overwrite=True)

        if trim:
            fitsutils.trim(outname, overwrite=True)



def cli():
    from argparse import ArgumentParser

    ps = ArgumentParser(description="Simple VLA primary beam correction of an image.")

    ps.add_argument("image", help="Image to PB correct.")
    ps.add_argument("-f", "--freq", type=float, default=None, 
        help="Frequency in GHz of image. If unset, will be read from the image.")
    ps.add_argument("-c", "--cutoff", default=None, type=float,
        help="PB map values below this value are set to NaN. By default no clipping is done.")
    ps.add_argument("-o", "--outname", default=None, type=str)
    ps.add_argument("-bo", "--beam_outname", default=None, type=str, 
        help="Output name for primary beam file. If unset not PB map is written to disk.")
    ps.add_argument("-p", "--pointing", nargs=2, default=None, type=float,
        help="Pointing centre in degrees if not at image centre.")
    ps.add_argument("-t", "--trim", action="store_true",
        help="Switch to trim image to the primary beam clip.")
    ps.add_argument("--model", 
        choices=["2018", "2022", "2023"],
        default="2023",
        help="Model to use. Default 2023 (http://www.ncra.tifr.res.in/ncra/gmrt/gmrt-users/beam-shape-v1-29nov2023.pdf"
    )

    args = ps.parse_args()

    pbcorrect(image=args.image,
        freq=args.freq,
        outname=args.outname,
        cutoff=args.cutoff,
        beam_outname=args.beam_outname,
        pcentre=args.pointing,
        trim=args.trim,
        model=args.model
    )
